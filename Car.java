class Car
{
	
	private String make, model, color;
	private double mileage, distanceTravelled;
	private boolean isWorkingFine;
	private int moneyPaidForFuel;
	private int fuelMagnitude;

	public Car(boolean isWorkingFine)
	{
		this.isWorkingFine = isWorkingFine;
		this.make = make;
		this.model = model;
		this.color = color;
	}

	public void setRefuel(int moneyPaidForFuel)
	{
		this.moneyPaidForFuel += moneyPaidForFuel;
		this.fuelMagnitude += moneyPaidForFuel/2;
	}

	public String giveIndicators(){
		this.brakeLights();
		return "tick tick tick tick";
	}

	public String brakeLights(){
		return "tinggg";
	}

	public boolean hasFuel(){
		if(this.fuelMagnitude <= 0)
		{
			return false;
		}
		return true;
	}
	
	public double getMileage(){
		return this.distanceTravelled/this.moneyPaidForFuel;
	}

	public String applyBrakes(){
		return "sssssssssssshhhhhhhhssss";
	}
	public String turnOffEngine(){
		return "grrrrrrr";
	}

	public void stopCar(){
		this.turnOffEngine();
		this.applyBrakes();
	}

	public String getDriveSound(){
		this.fuelMagnitude -= 7;
		this.distanceTravelled += 50;
		if(this.fuelMagnitude > 0){
			if(isWorkingFine && this.distanceTravelled<200)
			{
				return "Wroom Wroom Wroom !\n";
			}
			else{
			this.isWorkingFine = false;
			return "ghhhhsssh Wroom ghsh ghssh wroom!\n";
			}
		}
		else
		{
			return "ghhhhsssh ghsssh\n";
		}
	}
}