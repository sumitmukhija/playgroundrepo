class Runner
{
	static boolean wantToRunAgain;
	public static void main(String ar[])
	{
		Car c = new Car(true);
		c.setRefuel(40);
		if(c.hasFuel())
		{
			do{
				System.out.print(c.getDriveSound()+" "+c.hasFuel());
				wantToRunAgain = true;
			}while(wantToRunAgain && c.hasFuel());
			c.stopCar();	
		}
		else
		{
			System.out.println("You ran out of fuel!");
		}
	}
}